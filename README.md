# Speleosaurus

Speleosaurus est le [thésaurus des grottes ornées du Paléolithique](https://opentheso.huma-num.fr/opentheso/?idt=th459) en cours de développement. Ce thésaurus est développé avec l'instance [Opentheso](https://opentheso.huma-num.fr/) hébergée par la [TGIR Huma-Num](https://www.huma-num.fr/). Les différentes versions peuvent être téléchargées au format rdf sur le [dépôt git du projet](https://gitlab.huma-num.fr/grotte-chauvet/speleosaurus).

## Objectif

Le thésaurus des grottes ornées du Paléolithique fournit un référentiel contrôlé de toponymes pertinent pour l'étude des cavités.

## Contenu

Le thésaurus des grottes ornées du Paléolithique comporte les catégories de premier niveau suivantes, utilisées pour regrouper les concepts :

- **lieux :** étendues dans l'espace déterminées par référence à la position d'objets et identifiées par une ou plusieurs appellations.

## Contribuer

Le thésaurus grottes ornées du Paléolithique est maintenu par Nicolas Frerebeau. Le signalement d'erreurs ou les propositions d'améliorations ou de modifications peuvent être soumises en ouvrant un [ticket sur le dépôt git du projet](https://gitlab.huma-num.fr/grotte-chauvet/speleosaurus/-/issues).

